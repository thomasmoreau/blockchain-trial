# Description
- Project to better understand how a blockchain works, from its creation to its use. The goal of this project is not to create a useful system but simply educational on the technical level.
- Blockchain can be seen as an open DB shared among multiple peers, the key difference is that a blockchain is not based on trust, indeed, in a normal distributed DB system, each node's data will be considered as trustworthy.

# Elements
## Block
- Our block from the blockchain will need to consist of three different fields. 
- Since the main appeal of the blockchain is how secure it is, we are going to need some hashing. Hashing wouldn't be particularly useful unless there was some data inside that was worth protecting or preserving in some fashion, so we also need some data. 
- The last element will be how our blocks chain together, so we will also want the previous hash from the block that was before us.

## Chain
- The chain portion of a blockchain is just the collection of blocks all together. I think that sounds like a good use case for an array.
