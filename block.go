package main

import (
	"bytes"
	"crypto/sha256"
)

// Block : Struct to stock information for a block part of the chain
type Block struct {
	Hash     []byte
	Data     []byte
	PrevHash []byte
}

func (b *Block) DeriveHash() {
	// This will join our previous block's relevant info with the new blocks
	info := bytes.Join(
		[][]byte{b.Data, b.PrevHash},
		[]byte{},
	)

	//This performs the actual hashing algorithm
	hash := sha256.Sum256(info)

	b.Hash = hash[:]
}
