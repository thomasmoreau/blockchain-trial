package main

import "fmt"

var (
	dataList  []string
	blockList []*Block
)

func main() {
	blockList = []*Block{}
	dataList = []string{"test1", "test2", "test3", "test_last"}

	// ------------------------- Test
	for _, data := range dataList {
		blockList = append(blockList, &Block{Data: []byte(data)})
	}

	for i := 0; i < len(blockList)-1; i++ {
		// If not first element of list, add Hash of previous block
		if i != 0 {
			blockList[i].PrevHash = blockList[i-1].Hash
		}
		// Derive hash of each block
		blockList[i].DeriveHash()
		// Check values
		fmt.Printf("%+v \n\n", blockList[i])
	}
	// --------------------------
}
