check:
	go fmt ./...
	go vet ./...
	go mod tidy
	go test ./...

update:
	git add *
	git add .gitlab-ci.yml
	
