// Import library
local jobs = import "./lib/jobs.libsonnet";

// Declare variables
local kanikoStageName = "build";
local kanikoImageTag ="gcr.io/kaniko-project/executor:debug";
local kanikoTagsList = [""];

// Payload
{
    build: jobs.kanikoDockerImageBuild(kanikoImageTag, kanikoTagsList, kanikoStageName),
    run: {
        script: ['echo Hello World !'],
    },
}