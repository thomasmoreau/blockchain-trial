{
    // Job to build image
    kanikoDockerImageBuild(kanikoImage, tagsList, stageName)::{
        tags: tagsList,
        stage: stageName,
        image:{
            name: kanikoImage,
            entrypoint: [''],
        },
        script:[
            'echo "{\\"auths\\":{\\"$CI_REGISTRY\\":{\\"username\\":\\"$CI_REGISTRY_USER\\",\\"password\\":\\"$CI_REGISTRY_PASSWORD\\"}}}" > /kaniko/.docker/config.json',
        ],
    },    
}
