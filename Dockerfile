# BUILD STAGE
FROM golang:1.17-alpine3.15 AS build_stage

WORKDIR /project/

COPY ./*.go ./
COPY ./go.mod ./

RUN go get .
RUN go build -o blockchain_trial .

# RUN STAGE
FROM scratch AS run_stage
WORKDIR /project
COPY --from=build_stage /project/blockchain_trial ./

CMD ["./blockchain_trial"]